# D-Bot CoreXY Marlin Firmware

This repo holds modifications for Marlin 1.1.9 release firmware to make it suitable with Ramps 1.4 D-Bot CoreXY build. 


Custom parts: https://www.thingiverse.com/thing:3080314

Only seems to be building with Arduino IDE 1.8.0